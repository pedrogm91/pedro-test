<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Type;
use Illuminate\Http\Request;
use Inertia\Inertia;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = Customer::all();
        return Inertia::render(
            'Customers/Index',
            [
                'customers' => $customers
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render(
            'Customers/Create'
         );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'organization' => 'required',
            'phone' => 'required',
            'type_id' => 'required'
         ]);
         Customer::create([
            'name' => $request->name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'organization' => $request->organization,
            'phone' => $request->phone,
            'type_id' => $request->type_id,
         ]);
         sleep(1);
         return redirect()->route('customer.index')->with('message', 'Customer Created Succesfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        return Inertia::render(
            'Customers/Edit',
            [
               'customer' => $customer
            ]
         );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        $request->validate([
            'name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'organization' => 'required',
            'phone' => 'required',
            'type_id' => 'required'
         ]);
         $customer->name = $request->name;
         $customer->last_name = $request->last_name;
         $customer->email = $request->email;
         $customer->organization = $request->organization;
         $customer->phone = $request->phone;
         $customer->type_id = $request->type_id;
         $customer->save();
         sleep(1);
         return redirect()->route('customer.index')->with('message', 'Customer Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        $customer->delete();
        sleep(1);
        return redirect()->route('customer.index')->with('message', 'Customer Delete Successfully');
    }
}
