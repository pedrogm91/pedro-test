<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    use HasFactory;


    // relations
    public function customers()
    {
        return $this->hasMany(Customer::class);
    }
}
