<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(10)->create();

        \App\Models\User::factory()->create([
            'name' => 'Pedro',
            'email' => 'pedrogm91@gmail.com',
        ]);

        \App\Models\Type::create([
            'name' => 'Natural',
            'description' => 'Cliente de tipo natural',
        ]);
        \App\Models\Type::create([
            'name' => 'Empresarial',
            'description' => 'Cliente de tipo empresarial',
        ]);
        \App\Models\Type::create([
            'name' => 'Otros',
            'description' => 'Otros tipos de clientes',
        ]);
    }
}
